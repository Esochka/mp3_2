import dao.SongDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.DirectoryChooser;
import model.Song;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    private ObservableList<String> genge = FXCollections.observableArrayList();
    @FXML
    private ComboBox<String> comboGenre = new ComboBox<String>();

    ObservableList<String> artist = FXCollections.observableArrayList();
    @FXML
    private ComboBox<String> comboArtist = new ComboBox<String>();

    @FXML
    private TableView<Song> table = new TableView<>();

    @FXML
    private TableColumn<Song,String> columSong;

    @FXML
    private TableColumn<Song,String> columArtist;

    @FXML
    private TableColumn<Song,String> columGenge;

    @FXML
    private TableColumn<Song,String> columAlbum;

    MediaPlayer player;

    public void initialize() {
        columSong.setCellValueFactory(new PropertyValueFactory<>("name"));
        columArtist.setCellValueFactory(new PropertyValueFactory<>("artist"));
        columGenge.setCellValueFactory(new PropertyValueFactory<>("genre"));
        columAlbum.setCellValueFactory(new PropertyValueFactory<>("album"));
       // columYear.setCellValueFactory(new PropertyValueFactory<>("year"));
       // columTime.setCellValueFactory(new PropertyValueFactory<>("time"));

        table.setRowFactory( tv -> {
            TableRow<Song> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1  ) {
                    String uriString = new File(table.getSelectionModel().getSelectedItem().getLink().replaceAll("\\?", "")).toURI().toString();
                    player = new MediaPlayer(new Media(uriString));
                    player.play();
                }
            });
            return row ;
        });
    }

    @FXML
    private void open(){

       final DirectoryChooser directoryChooser = new DirectoryChooser();
       File dir = directoryChooser.showDialog(null);
        final char dm = (char) 34;
        List<String> genres = SongDao.setLink(String.valueOf(dir).replaceAll("\\\\","/"));
        System.out.println(genres.toString());
        for (String genre:genres) {

            genge.add(genre);
        }
        comboGenre.setItems(genge);
    }


    @FXML
    private void actionComboGenre(){
        artist.clear();
        List<String>  artists = SongDao.getArtistsByGenre(comboGenre.getValue());
        for (String artist:artists) {
            this.artist.add(artist);
        }

        artist.addAll();
        comboArtist.setItems(artist);
    }

    @FXML
    private void actionComboArtist(){
        List<Song> songs = SongDao.getSongByArtist(comboGenre.getValue(), comboArtist.getValue());
        List<Song> songDtoList = new ArrayList<>();
        for (Song song: songs) {
            songDtoList.add(new Song(song.getName(), song.getGenre(), song.getArtist(), song.getLink(), song.getTime(), song.getAlbum(), song.getYear()));
        }
        ObservableList<Song> songData = FXCollections.observableArrayList();
        songData.addAll(songDtoList);
        table.setItems(songData);
    }


    @FXML
    private void play(){
        player.play();
    }

    @FXML
    private void stop(){
            player.pause();
    }

}
