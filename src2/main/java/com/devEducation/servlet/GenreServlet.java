package com.devEducation.servlet;

import com.devEducation.config.Gonfig;
import com.devEducation.json.GetJson;
import com.devEducation.model.Album;
import com.devEducation.model.Artist;
import com.devEducation.model.Genre;
import com.devEducation.model.Song;
import com.devEducation.service.AlbumService;
import com.devEducation.service.ArtistService;
import com.devEducation.service.GenreService;
import com.devEducation.service.SongService;
import com.google.gson.Gson;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@WebServlet(name = "ArtistsSong", urlPatterns = "/addSong")
public class GenreServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-16");

        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        EntityManagerFactory entityManagerFactory = Gonfig.getEntityManager();


        String url = request.getParameter("url");
        System.out.println(url);
        File file = new File(url);
        String[] musics = file.list();

        try {
            int i = 0;
            String stringList = new String();
            Set<Genre> result = null;
            EntityManager entityManager1 = entityManagerFactory.createEntityManager();
            for (i = 0; i < musics.length ; i++) {


                EntityManager entityManager2 = entityManagerFactory.createEntityManager();
                EntityManager entityManager3 = entityManagerFactory.createEntityManager();
                EntityManager entityManager4 = entityManagerFactory.createEntityManager();



                Mp3File mp3File = new Mp3File(url + "/" + musics[i]);
                String name = musics[i];
                String artist = mp3File.getId3v2Tag().getArtist();
                String link = url + "/" + musics[i];
                String genre = mp3File.getId3v2Tag().getGenreDescription();
                String album = mp3File.getId3v2Tag().getAlbum();
                String time = String.valueOf(mp3File.getId3v2Tag().getLength());
                String year = mp3File.getId3v2Tag().getYear();

                Genre genre1 = new Genre(genre);
                entityManager2.getTransaction().begin();
                entityManager2.persist(genre1);
                entityManager2.getTransaction().commit();


                Artist artist1 = new Artist(artist, genre1);
                entityManager2.getTransaction().begin();
                entityManager2.persist(artist1);
                entityManager2.getTransaction().commit();

                Album album1 = new Album(artist1, album, year);
                entityManager3.getTransaction().begin();
                entityManager3.persist(album1);
                entityManager3.getTransaction().commit();

                Song song1 = new Song(name, genre1, artist1, album1, link, time);
                entityManager4.getTransaction().begin();
                entityManager4.persist(song1);
                entityManager4.getTransaction().commit();


            }

            entityManager1.getTransaction().begin();
            result = new HashSet<>(entityManager1.createQuery("from Genre", Genre.class).getResultList());
            entityManager1.getTransaction().commit();
            System.out.println(result);


            for (Genre ganr : result) {
                stringList += (ganr.getGenre()) + ",";
            }
            out.print(new Gson().toJson(stringList));

        } catch (UnsupportedTagException e) {
            e.printStackTrace();
        } catch (InvalidDataException e) {

            e.printStackTrace();
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}


