package com.devEducation.model;

import java.util.Objects;

public class SongDto {
    private String name;

    private String genre;

    private String artist;

    private String link;

    private String time;

    private String album;

    private String year;

    public SongDto(String name, String genre, String artist, String link, String time, String album, String year) {
        this.name = name;
        this.genre = genre;
        this.artist = artist;
        this.link = link;
        this.time = time;
        this.album = album;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongDto songDto = (SongDto) o;
        return Objects.equals(name, songDto.name) &&
                Objects.equals(genre, songDto.genre) &&
                Objects.equals(artist, songDto.artist) &&
                Objects.equals(link, songDto.link) &&
                Objects.equals(time, songDto.time) &&
                Objects.equals(album, songDto.album) &&
                Objects.equals(year, songDto.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, genre, artist, link, time, album, year);
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", artist='" + artist + '\'' +
                ", link='" + link + '\'' +
                ", time='" + time + '\'' +
                ", album='" + album + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
