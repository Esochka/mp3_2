package com.devEducation.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Artist")
public class Artist {

    @Id
    @GeneratedValue
    private Integer id;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Artist{");
        sb.append("id=").append(id);
        sb.append(", genre=").append(genre);
        sb.append(", name='").append(name).append('\'');
        sb.append(", albums=").append(albums);
        sb.append(", songs=").append(songs);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return Objects.equals(name, artist.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }

    @OneToMany
    private List<Genre> genre = new ArrayList<>();

    private String name;
    @OneToMany
    private List<Album> albums = new ArrayList<>();

    @OneToMany
    private List<Song> songs = new ArrayList<>();


    public Artist() {
    }

    public Artist(String name, Genre genre) {
        this.name = name;
        this.genre.add(genre);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtists() {
        return name;
    }

    public void setArtists(String artists) {
        this.name = artists;
    }


}
