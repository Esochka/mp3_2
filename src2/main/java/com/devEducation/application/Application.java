package com.devEducation.application;

import com.devEducation.model.Album;
import com.devEducation.model.Artist;
import com.devEducation.model.Genre;
import com.devEducation.model.Song;
import com.devEducation.service.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class Application {



    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("train");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        EntityManager entityManager2 = entityManagerFactory.createEntityManager();

        Genre genre1 = new Genre("genre");
        entityManager.getTransaction().begin();
        entityManager.persist(genre1);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        List<Genre> result = entityManager.createQuery("from Genre",Genre.class).getResultList();
        entityManager.getTransaction().commit();
        System.out.println(result);




        Artist artist = new Artist("artist" , genre1);
        entityManager2.getTransaction().begin();
        entityManager2.persist(artist);
        entityManager2.getTransaction().commit();

        List<Artist> artistList = new ArrayList<>();

        entityManager2.getTransaction().begin();
        List<Artist> result2 = entityManager2.createQuery("SELECT a FROM Artist a ", Artist.class).getResultList();
        entityManager2.getTransaction().commit();
        for (Artist a: result2) {
            List<Genre> genres = a.getGenre();
            for (Genre genres2: genres) {
                if (genres2.getGenre().equals("genre")){
                    artistList.add(a);
                    System.out.println("asd");
                }
            }
        }

        System.out.println(result2);


     //   System.out.println(MainService.getAllGenres());




    }


}
