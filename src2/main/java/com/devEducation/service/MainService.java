package com.devEducation.service;

import com.devEducation.config.Gonfig;
import com.devEducation.model.Artist;
import com.devEducation.model.Genre;
import com.devEducation.model.Song;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class MainService {

    private static EntityManager entityManager;

    static {

            entityManager = Gonfig.getEntityManager().createEntityManager();

    }


    public static List<Genre> getAllGenres(){
        entityManager.getTransaction().begin();
       List<Genre> result = entityManager.createQuery("from Genre",Genre.class).getResultList();
        entityManager.getTransaction().commit();
       return result;
    }

    public static List<Artist> getAllArtistByGenre(Genre genre){
        TypedQuery<Artist> query =
                entityManager.createQuery("SELECT a FROM Artist a where a.genre="+genre.getGenre(), Artist.class);

        List<Artist> results = query.getResultList();
        return results;
    }

    public static List<Song> getAllSongsByGenreAndArtist(Genre genre , Artist artist){
        TypedQuery<Song> query =
                entityManager.createQuery("SELECT s FROM Song s where s.genre="+genre.getGenre()+" and s.artist="+artist.getName(), Song.class);

        List<Song> results = query.getResultList();
        return results;

    }

}
