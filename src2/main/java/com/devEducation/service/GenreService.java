package com.devEducation.service;

import com.devEducation.config.Gonfig;
import com.devEducation.model.Artist;
import com.devEducation.model.Genre;

import javax.persistence.EntityManager;

public class GenreService {

    private  EntityManager entityManager = null;

    public GenreService() throws ClassNotFoundException {
        entityManager= Gonfig.getEntityManager().createEntityManager();
    }

    public void save(Genre genre) {
        entityManager.getTransaction().begin();
        entityManager.persist(genre);
        entityManager.getTransaction().commit();
    }
}
